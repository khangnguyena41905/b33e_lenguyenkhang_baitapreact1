import React, { Component } from "react";
import Content from "./Content";
import Footer from "./Footer";
import Header from "./Header";
import Responsive from "./Responsive";

export default class MainComponent extends Component {
  render() {
    return (
      <div>
        <Responsive />
        <Header />
        <Content />
        <Footer />
      </div>
    );
  }
}
