import React, { Component } from "react";
import ContentItem from "./ContentItem";

export default class Content extends Component {
  render() {
    return (
      <div>
        <section className="pt-4">
          <div className="container px-lg-5">
            {/* Page Features*/}
            <div className="row gx-lg-5">
              <ContentItem />
              <ContentItem />
              <ContentItem />
              <ContentItem />
              <ContentItem />
              <ContentItem />
            </div>
          </div>
        </section>
      </div>
    );
  }
}
