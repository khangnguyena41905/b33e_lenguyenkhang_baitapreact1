import "./App.css";
import MainComponent from "./Component/MainComponent";

function App() {
  return (
    <div>
      <MainComponent />
    </div>
  );
}

export default App;
